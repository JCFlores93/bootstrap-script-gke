# Install docker and enable it.
echo "Verify if docker is already installed."
if docker version 2>&1 >> /dev/null ; then
    echo "Docker is already installed" 
else
    curl -fsSL get.docker.com -o get-docker.sh
    sh get-docker.sh
    usermod -aG docker $USER
    systemctl enable docker
    systemctl restart docker
    docker --version
fi
