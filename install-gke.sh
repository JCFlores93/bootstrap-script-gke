#!/bin/bash

set -e

ROOT=$(pwd)
gcloud projects list
PROJECT_ID=$(gcloud config get-value project)

read -p "Please enter your PROJECT_ID [${PROJECT_ID}]: " GCLOUD_PROJECT
GCLOUD_PROJECT=${GCLOUD_PROJECT:-$PROJECT_ID}
echo $GCLOUD_PROJECT

gcloud config set project $GCLOUD_PROJECT

# Install and configure gcloud components
# Enable cloud apis
echo "Enable cloud apis..."
declare -a apis=( "compute" "servicenetworking" "cloudresourcemanager" "container" )
for api in "${apis[@]}"; do
    gcloud services enable "${api}.googleapis.com"
done
gcloud components install kubectl
echo "Done, gcloud apis were enabled."

# Create an service account
gcloud iam service-accounts list
echo -n "Do you want to create a new service account? (Please enter between 6 and 30 characteres)[y/n]"
read answer

if [ "$answer" != "${answer#[Yy]}" ]; then
        echo "Insert your service account name"
        read SERVICE_ACCOUNT_NAME
        gcloud iam service-accounts create $SERVICE_ACCOUNT_NAME
else
        echo "Insert your service account name"
        read SERVICE_ACCOUNT_NAME
fi

echo $SERVICE_ACCOUNT_NAME
                           

# Associate service account roles
echo "Associate needed apis..."
declare -a roles=( "container.admin" "compute.admin" "iam.serviceAccountUser" "resourcemanager.projectIamAdmin" "owner" "storage.admin" )
for rol in "${roles[@]}"; do
    gcloud projects add-iam-policy-binding $GCLOUD_PROJECT --member serviceAccount:${SERVICE_ACCOUNT_NAME}@${GCLOUD_PROJECT}.iam.gserviceaccount.com --role "roles/${rol}"
done
echo "Done, roles for your service account were added."

# Create a repo to deploy
if [ ! -d "./demo-project" ];then
    mkdir -p demo-project && cd demo-project
else 
    cd demo-project
fi

ROOT_PROJECT=$(pwd)

echo -n "Enter a key name:"
read KEY_NAME

# Get the key info
gcloud iam service-accounts keys create "${KEY_NAME}.json" --iam-account=${SERVICE_ACCOUNT_NAME}@${GCLOUD_PROJECT}.iam.gserviceaccount.com

# Create bucket for terrafrom.state
LOCATION=$(gcloud config get-value compute/region)
echo -n "Do you want to create a new bucket?[y/n]"
read answer

if [ "$answer" != "${answer#[Yy]}" ]; then
    echo "Insert the bucket name"
    read BUCKET_NAME
    gsutil mb -p $GCLOUD_PROJECT -c regional -l $LOCATION gs://${BUCKET_NAME}/
else
    gsutil ls
    echo "Insert the bucket name"
    read BUCKET_NAME    
fi

echo $BUCKET_NAME

# Activar versionamientos del bucket
# Versionamiento del bucket
gsutil versioning set on gs://${BUCKET_NAME}/

# Dar los permisos de iam
# Grant read and write permissions on iam
gsutil iam ch serviceAccount:${SERVICE_ACCOUNT_NAME}@${GCLOUD_PROJECT}.iam.gserviceaccount.com:legacyBucketWriter gs://${BUCKET_NAME}/

gcloud components install kubectl
gcloud auth configure-docker
# Desplegar terraform
# Create a repo to deploy
if [ -d "demo-gke" ];then
    rm -rf demo-gke
fi

git clone https://gitlab.com/JCFlores93/demo-gke.git
cat "${KEY_NAME}.json" > demo-gke/"${KEY_NAME}.json"
echo "============================="
 pwd
echo "============================="
cat ../variables.auto.tfvars > demo-gke/variables.auto.tfvars
echo -e " \n" >> demo-gke/variables.auto.tfvars
echo "credentials = \"${KEY_NAME}.json\"" >>  demo-gke/variables.auto.tfvars
echo "project_id = \"${GCLOUD_PROJECT}\"" >>  demo-gke/variables.auto.tfvars
echo "service_account = \"${SERVICE_ACCOUNT_NAME}@${GCLOUD_PROJECT}.iam.gserviceaccount.com\"" >>  demo-gke/variables.auto.tfvars
cd demo-gke

# Replace keyfile in provider.tf
sed -i "s~credentials.*~credentials = file(\"${KEY_NAME}.json\")~g" provider.tf
sed -i "s~credentials.*~credentials = \"${KEY_NAME}.json\"~g" terraform.tf

# Replace bucket state in terraform.tf
sed -i "s~bucket.*~bucket = \"${BUCKET_NAME}\"~g" terraform.tf

# setup repository


echo "Formating..."
terraform fmt
echo "Terraform init..."
terraform init
echo "Terraform plan..."
terraform plan -out=tfplan -input=false
echo "Terraform apply..."
terraform apply -input=false tfplan

# list containers
echo "List containers..."
gcloud container clusters list

# Set credentials
echo "Set up credentials..."
gcloud container clusters get-credentials gke-cluster --zone us-east4

# desplegar servicios
# Download repo de python

cd ROOT_PROJECT/

# Download rest api 
if [ -d "demo-rest-api-gke" ];then
    rm -rf demo-rest-api-gke
fi
git clone https://gitlab.com/JCFlores93/demo-rest-api-gke.git

IMAGE_NAME=$( cat ../variables.auto.tfvars | grep "registry*" | awk -F'=' '{ print $2 }' | tr -d '"' )
REGION=$( cat ../variables.auto.tfvars | grep "region*" | awk -F'=' '{ print $2 }' | tr -d '"' )
cd demo-rest-api-gke

# Create regional IP to be applied to the service
gcloud compute addresses create demo-ip --region $REGION
IP_ADDRESS=$(gcloud compute addresses describe demo-ip --region $REGION | grep -w "address:" | awk -F':' '{ print $2 }' | tr -d '[:space:]')

REGISTRY_NAME="gcr.io/${GCLOUD_PROJECT}/${IMAGE_NAME}:latest"

sed -i "s~docker build.*~docker build -t ${IMAGE_NAME}:latest .~g" Makefile
sed -i "s~docker run.*~docker run -d -p 5000:5000 ${IMAGE_NAME}~g" Makefile
sed -i "s~docker tag.*~docker tag ${IMAGE_NAME} ${REGISTRY_NAME}~g" Makefile
sed -i "s~docker push.*~docker push ${REGISTRY_NAME}~g" Makefile

sed -i "s~loadBalancerIP.*~loadBalancerIP: \"${IP_ADDRESS}\"~g" ./src/deployment/service.yaml

echo "Starting to build the image..."
make build
echo "The image was built."
echo "Adding the tag"
make tag
echo "Pushing the image..."
make push
echo "Done."

echo "Starting to deploy into k8s..."
cd src/deployment
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml

echo "Done."

cd $ROOT
cat "Service deployed in : ${IP_ADDRESS}" > output.txt



