# Defaul config
region             = "us-east4"
zones              = ["us-east4-a", "us-east4-b"]
name               = "gke-cluster"
machine_type       = "n1-standard-1"
min_count          = 1
max_count          = 2
disk_size_gb       = 10
initial_node_count = 2
registry_name      = "flaskdemo"
