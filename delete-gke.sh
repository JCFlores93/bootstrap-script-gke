#!/bin/bash

set -e

if [ ! -d "./demo-project" ];then
    echo "There's no elements to delete"
    echo "Done..."
else 
    cd demo-project
    ROOT_PROJECT=$(pwd)
    cd demo-rest-api-gke/
    kubectl delete -f src/deployment/deployment.yaml
    kubectl delete -f src/deployment/service.yaml
    cd ../demo-rest-api-gke
    terraform destroy -auto-approve
    REGION=$( cat ./variables.auto.tfvars | grep "region*" | awk -F'=' '{ print $2 }' | tr -d '"' )
    gcloud compute addresses delete demo-ip --region $REGION
    cd ..
    rm -rf demo-project
fi