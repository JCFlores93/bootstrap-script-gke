curl -o google-cloud-sdk.tar.gz https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-300.0.0-linux-x86_64.tar.gz
tar zxvf google-cloud-sdk.tar.gz google-cloud-sdk
./google-cloud-sdk/install.sh

gcloud compute project-info add-metadata \
    --metadata google-compute-default-region=us-east4,google-compute-default-zone=us-east4-a


gcloud init
