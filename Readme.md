# Pre-requisites

1. setup-terraform.sh -> To install terraform(if you need it).
2. setup-docker.sh    -> To install docker (if you need it).
3. setup-gcloud.sh    -> To install gcloud on ubuntu(if you need it).

# Modify variables for the cluster
* Modify the files named variables.auto.tfvars to adjust it to your needs.

# Default config
region             = "us-east4"
zones              = ["us-east4-a", "us-east4-b"]
name               = "gke-cluster"
machine_type       = "n1-standard-1"
min_count          = 1
max_count          = 2
disk_size_gb       = 10
initial_node_count = 2
# This registry will be deployed on GCP
registry_name      = "flask-demo"

# Deploy application
make create

# Delete application's resources
make delete

# Output
make output

